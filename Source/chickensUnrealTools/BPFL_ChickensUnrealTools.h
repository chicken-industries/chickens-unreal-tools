﻿// Copyright 2024 Sebastian Hennemann. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/SCS_Node.h"
#include "Engine/SimpleConstructionScript.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BPFL_ChickensUnrealTools.generated.h"


UCLASS()
class CHICKENSUNREALTOOLS_API UBPFL_ChickensUnrealTools : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
	static UObject* GetClassDefaultObject(TSubclassOf<UObject> ObjectClass);
	
	UFUNCTION(BlueprintCallable)
	static TArray<UActorComponent*> FindDefaultComponentsByClass(UClass* ActorClass, UClass* ComponentClass)
	{
		TArray<UActorComponent*> Components;
		FindDefaultComponentsByClass(ActorClass, ComponentClass, Components);
		return Components;
	}

	template <class T>
	static void FindDefaultComponentsByClass(UClass* ActorClass, UClass* ComponentClass, TArray<T*>& OutFoundComponents)
	{
		OutFoundComponents.Empty();
		/*
		const UBlueprintGeneratedClass* ActorBlueprintGeneratedClass = Cast<UBlueprintGeneratedClass>(ActorClass);
    
		const TArray<USCS_Node*>& ActorBlueprintNodes = ActorBlueprintGeneratedClass->SimpleConstructionScript->GetAllNodes();
    
		for (USCS_Node* Node : ActorBlueprintNodes)
		{
			if (UClass::FindCommonBase(Node->ComponentClass, ComponentClass) == ComponentClass)
			{
				OutFoundComponents.Emplace(Cast<T>(Node->ComponentTemplate));
			}
		}
		*/
	
		if (!IsValid(ActorClass) || !IsValid(ComponentClass))
		{
			return;
		}

		AActor* ActorCDO = ActorClass->GetDefaultObject<AActor>();
		ActorCDO->GetComponents(ComponentClass, OutFoundComponents);
		
		// Check blueprint nodes. Components added in blueprint editor only (and not in code) are not available from
		// CDO.
		UBlueprintGeneratedClass* RootBlueprintGeneratedClass = Cast<UBlueprintGeneratedClass>(ActorClass);

		// Go down the inheritance tree to find nodes that were added to parent blueprints of our blueprint graph.
		do
		{
			UBlueprintGeneratedClass* ActorBlueprintGeneratedClass = Cast<UBlueprintGeneratedClass>(ActorClass);

			if (!ActorBlueprintGeneratedClass)
			{
				return;
			}

			const TArray<USCS_Node*>& ActorBlueprintNodes = ActorBlueprintGeneratedClass->SimpleConstructionScript->GetAllNodes();

			for (USCS_Node* Node : ActorBlueprintNodes)
			{
				if (Node->ComponentClass->IsChildOf(ComponentClass))
				{
					OutFoundComponents.Add(Node->GetActualComponentTemplate(RootBlueprintGeneratedClass));
				}
			}

			ActorClass = Cast<UClass>(ActorClass->GetSuperStruct());

		} while (ActorClass != AActor::StaticClass());
	}

	UFUNCTION(BlueprintPure)
	static bool GameplayTagContainerMatchRequired(const FGameplayTagContainer& ContainerToTest, const FGameplayTagContainer& RequiredTags, const FGameplayTagContainer& ForbiddenTags)
	{
		return ContainerToTest.HasAllExact(RequiredTags) && !ContainerToTest.HasAnyExact(ForbiddenTags);
	}
};
