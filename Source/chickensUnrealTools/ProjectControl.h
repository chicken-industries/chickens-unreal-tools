#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProjectControl.generated.h"

UCLASS()
class CHICKENSUNREALTOOLS_API UProjectControl : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	//Returns the project version set in the 'Project Settings' > 'Description' section of the editor
	UFUNCTION(BlueprintPure, Category = "Project")
		static FString GetProjectVersionString();
};