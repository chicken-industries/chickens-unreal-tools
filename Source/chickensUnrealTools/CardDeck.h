#pragma once


template <typename T>
class TCardDeck
{
public:
	TArray<T> Cards;

	// This function is already implemented for generic TArrays but i could not get it to work :(
	// https://docs.unrealengine.com/en-US/API/Runtime/Engine/Kismet/UKismetArrayLibrary/Array_Shuffle/index.html
	void Shuffle()
	{
		int32 LastIndex = Cards.Num() - 1;

		for (int32 i = 0; i <= LastIndex; ++i)
		{
			int32 Index = FMath::RandRange(i, LastIndex);

			if (i != Index)
				Cards.Swap(i, Index);
		}
	}
	
	T DrawCardFromTop()
	{
		if (Cards.Num() > 0)
			return Cards.Pop();

		return T();
	}

	T DrawCardFromBottom()
	{
		if (Cards.Num() > 0)
		{
			T DrawnCard = Cards[0];
			Cards.RemoveAt(0);

			return DrawnCard;
		}

		return T();
	}
};


