#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActorSlotsComponent.generated.h"


UENUM(BlueprintType)
enum class ESlotStatus : uint8
{
	Open				UMETA(DisplayName = "Open"),
	Closed				UMETA(DisplayName = "Closed"),
	Occupied			UMETA(DisplayName = "Occupied"),
	Locked				UMETA(DisplayName = "Locked"),
};

USTRUCT(BlueprintType)
struct CHICKENSUNREALTOOLS_API FSlotData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	ESlotStatus Status = ESlotStatus::Locked;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	AActor* Actor = nullptr;
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FED_UpdateSlot);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class CHICKENSUNREALTOOLS_API UActorSlotsComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Replicated)
	TArray<FSlotData> Slots;

public:	
	
	UPROPERTY(BlueprintAssignable, Category = "Event Dispatchers")
	FED_UpdateSlot ED_UpdateSlot;

public:	
	UActorSlotsComponent();

	UFUNCTION(BlueprintPure)
	TArray<FSlotData> const& GetSlotData() const
	{
		return Slots;	
	}

	UFUNCTION(NetMulticast, unreliable)
	void BroadcastSlotUpdate_Multicast() const;

	void KickOutActor(int Index);
	
	UFUNCTION(BlueprintCallable)
	void OpenSlot(int Index);
	
	UFUNCTION(BlueprintCallable)
	void CloseSlot(int Index);
	
	void LockSlot(int Index);
	
	UFUNCTION(BlueprintCallable)
	void OccupySlot(int Index, AActor* NewActor);

private:
	void ClearSlot(int Index, ESlotStatus NewStatus);
	
};


UINTERFACE(MinimalAPI, Blueprintable)
class USlottableActorInterface : public UInterface
{
	GENERATED_BODY()
};

class CHICKENSUNREALTOOLS_API ISlottableActorInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = SlotComponentInterface)
	void OnRemovedFromSlot(UActorSlotsComponent* SlotComponent);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = SlotComponentInterface)
	void OnAssignedToSlot(UActorSlotsComponent* SlotComponent);
};
