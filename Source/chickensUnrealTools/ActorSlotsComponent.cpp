#include "ActorSlotsComponent.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Actor.h"

#include <assert.h>

UActorSlotsComponent::UActorSlotsComponent()
{
	SetIsReplicatedByDefault(true);
	PrimaryComponentTick.bCanEverTick = false;
}


void UActorSlotsComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(UActorSlotsComponent, Slots);
}

void UActorSlotsComponent::BroadcastSlotUpdate_Multicast_Implementation() const
{
	ED_UpdateSlot.Broadcast();
}

void UActorSlotsComponent::KickOutActor(int Index)
{
	auto& Actor = Slots[Index].Actor;
	
	if (Actor && Actor->Implements<USlottableActorInterface>())
		ISlottableActorInterface::Execute_OnRemovedFromSlot(Actor, this);
		
	Actor = nullptr;
}

void UActorSlotsComponent::OpenSlot(int Index)
{
	ClearSlot(Index, ESlotStatus::Open);
}

void UActorSlotsComponent::CloseSlot(int Index)
{
	ClearSlot(Index, ESlotStatus::Closed);
}

void UActorSlotsComponent::LockSlot(int Index)
{
	ClearSlot(Index, ESlotStatus::Locked);
}

void UActorSlotsComponent::OccupySlot(int Index, AActor* NewActor)
{
	assert(NewActor);
	
	KickOutActor(Index);
	
	auto& Slot = Slots[Index];
	
	if (NewActor->Implements<USlottableActorInterface>())
		ISlottableActorInterface::Execute_OnAssignedToSlot(NewActor, this);
	
	Slot.Actor = NewActor;
	Slot.Status = ESlotStatus::Occupied;
	
	BroadcastSlotUpdate_Multicast();
}

void UActorSlotsComponent::ClearSlot(int Index, ESlotStatus NewStatus)
{
	KickOutActor(Index);
	Slots[Index].Status = NewStatus;
	BroadcastSlotUpdate_Multicast();
}
