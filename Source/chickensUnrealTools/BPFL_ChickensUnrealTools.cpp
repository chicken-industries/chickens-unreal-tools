﻿// Copyright 2024 Sebastian Hennemann. All rights reserved.


#include "BPFL_ChickensUnrealTools.h"

UObject* UBPFL_ChickensUnrealTools::GetClassDefaultObject(TSubclassOf<UObject> ObjectClass)
{
	if (!IsValid(ObjectClass))
	{
		return nullptr;
	}
	
	return ObjectClass->GetDefaultObject();
}
