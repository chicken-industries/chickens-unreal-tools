﻿// Copyright 2024 Sebastian Hennemann. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/ObjectLibrary.h"

struct FDataManagement
{
	template <typename ObjectClass>
	static TArray<ObjectClass*> LoadAllObjectsOfType(FString LoadPath, UObjectLibrary* OutLibrary = nullptr)
	{
		TArray<ObjectClass*> Objects;

		OutLibrary = UObjectLibrary::CreateLibrary(ObjectClass::StaticClass(), true, GIsEditor);
		OutLibrary->LoadAssetDataFromPath(LoadPath);
		
		TArray<FAssetData> AssetDataArray;
		OutLibrary->GetAssetDataList(AssetDataArray);

		for (FAssetData AssetData : AssetDataArray)
		{
			Objects.Add(Cast<ObjectClass>(AssetData.GetAsset()));
		}

		Objects.Sort();

		return Objects;
	}
};
